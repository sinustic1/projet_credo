#include <Wire.h>
#include <heltec.h>

#define PIN_BATTERY_VOLTAGE 20 // Broche analogique utilisée pour mesurer la tension de la batterie

const float VOLTAGE_LOW = 0.0; // Seuil de tension basse en volts
const float VOLTAGE_HIGH = 3.3; // Seuil de tension haute en volts

void setup() {
  Heltec.begin(true /* DisplayEnable Enable*/, true /* Heltec.LoRa Disable*/, true /* Serial Enable */);
  Serial.begin(115200);
}

void loop() {
  float batteryVoltage = readBatteryVoltage(); // Lire la tension de la batterie
  Serial.print("Tension de la batterie: ");
  Serial.print(batteryVoltage);
  Serial.println(" V");

  int batteryPercentage = mapBatteryPercentage(batteryVoltage); // Convertir la tension en pourcentage
  Serial.print("Pourcentage de batterie: ");
  Serial.print(batteryPercentage);
  Serial.println(" %");

  // Vous pouvez ajouter ici le code pour envoyer la tension de la batterie et le pourcentage via LoRa, par exemple

  delay(5000); // Attente de 5 secondes avant de lire à nouveau la tension de la batterie
}

float readBatteryVoltage() {
  int rawValue = analogRead(PIN_BATTERY_VOLTAGE);
  float voltage = rawValue * (3.3 / 4095.0); // Convertir la valeur brute en tension (pour une carte Heltec LoRa V3 alimentée en 3.3V)
  return voltage;
}

int mapBatteryPercentage(float voltage) {
  // Cartographier la tension de la batterie en pourcentage en utilisant les seuils définis
  return map(voltage * 100, VOLTAGE_LOW * 100, VOLTAGE_HIGH * 100, 0, 100);
}
